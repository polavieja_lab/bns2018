# Machine Learning Notebooks

This branch works with PyTorch. Also, there is another branch using tensorflow called `keras`.

To run these files, in an environment with a working installation of PyTorch (`torch` and `torchvision`, check [how to install PyTorch](https://pytorch.org/get-started/locally/)), install the following dependencies:

```bash
python -m pip install -U numpy matplotlib ipython scikit-learn tqdm trajectorytools scipy gdown
```
